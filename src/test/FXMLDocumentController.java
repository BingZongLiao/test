/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import java.util.Random ;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    private Connection conn ;
    String[] FirstN = {"陳", "張", "廖", "李", "施", "盧", "蔡", "趙"} ;
    String[] LastN = {"泓羲","宣任", "奕嘉", "廷嘉", "秉宗", "忠澤", "育庭", "力銘", "智弓", "元渝"} ;
    
    
    @FXML
    private void handleButtonAction(ActionEvent event)
    {
        try {
            conn = DriverManager.getConnection("jdbc:derby://localhost:1527/contact", "test", "test") ;
            if(conn != null)
        {
            label.setText("Linked") ;
            doInsext() ;
        }
            
            }
        catch (SQLException ex) 
        {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            label.setText("Failed") ;
        }
    }
    
    private int doInsext()
    {
        int rows = 0 ;
        Random ran = new Random() ;
        String insert = "insert into TEST.COLLEAGUES values (?, ?, ?, ?, ?, ?, ?)" ;
        PreparedStatement stmt = null ;
        try {
            stmt = conn.prepareStatement(insert) ;
            stmt.setInt(1, (int) (Math.random()*(100-5)+5)) ;
            stmt.setString(2, FirstN[ran.nextInt(FirstN.length)]) ;
            stmt.setString(3, LastN[ran.nextInt(LastN.length)]) ;
            stmt.setString(4, "test") ;
            stmt.setString(5, "test") ;
            stmt.setString(6, "shenfail2266@0987987987") ;
            stmt.setInt(7, 8) ;
            rows = stmt.executeUpdate() ;
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            try {
                stmt.close() ;
                conn.close() ;
            } catch (SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return rows ;
            
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
